<?php
    /** ZONA HORARIA DE LA APLICACION */
    date_default_timezone_set("America/Montevideo");

    /***INSTANCIA DE LA VISTA CORRESPONDIENTE */
    if(isset($_GET['view']))
    {
        $URLController = 'controllers/'.$_GET['view'].'Controller.php';
        $Class = $_GET['view'].'Controller';  
        
        if(!file_exists($URLController))
        {
            $URLController = 'controllers/ErrorPageController.php';
            $Class = 'ErrorPageController';
        }        
    }   
    else
    {
        $URLController = 'controllers/ErrorPageController.php';
        $Class = 'ErrorPageController';   
    }

    require $URLController;   
    $View = new $Class;
    $View->getView();
?>