<?php
class IndexController
{
    private $config;
    private $session;
    private $page;
    private $tags;
    private $templateEngine;

    public function __construct()
    {
        require 'libs/PropFull.php';            //GETers and SETers de configuracion       
        require 'libs/TemplateEngine.php';      //Motor de plantillas   
        require 'libs/ConfigFile.php';          //Archivo con configuraciones.
        require 'libs/Session.php';             //Control de la session de usuario

        $this->config       = PropFull::singleton();
        $this->session      = new Session();
    }

    public function getView()
    {
        /***CARGAMOS EL OBJETO PARA TRABAJAR CON LAS TEMPLATE */
        $this->TemplateEngine = new TemplateEngine(true); 

        $this->page = 'Home';

        $this->tags = array(
                            '{TITULO}' => 'Inicio | Monitor',
                            '{TEST}' => 'Titulo',
                            '{TEST2}' => (isset($_GET['Otrovalor'])) ? $_GET['Otrovalor'] : 'SIN VALOR',
                            '{OPCIONES_SELECT}'=> $this->TemplateEngine->setSelect(array('op1'=>'op1','op2'=>'op2'))
                        );
        
        /*** MOSTRAMOS LA VISTA QUE CORRESPONDE */
        $this->TemplateEngine->getTemplate($this->page,$this->tags);         
    }
}
?>
