<?php
class ErrorPageController
{
    private $config;
    private $page;
    private $tags;
    private $templateEngine;

    public function __construct()
    {
        require 'libs/PropFull.php';            //GETers and SETers de configuracion       
        require 'libs/TemplateEngine.php';      //Motor de plantillas   
        require 'libs/ConfigFile.php';          //Archivo con configuraciones.

        $this->config       = PropFull::singleton(); 
    }

    public function getView()
    {
        /***CARGAMOS EL OBJETO PARA TRABAJAR CON LAS TEMPLATE */
        $this->TemplateEngine = new TemplateEngine(true);

        $this->page = 'ErrorPage';

        $this->tags = array(
                            '{TITULO}' => 'Error !'                           
                        );
        
        /*** MOSTRAMOS LA VISTA QUE CORRESPONDE */
        $this->TemplateEngine->getTemplate($this->page,$this->tags);         
    }
}
?>
