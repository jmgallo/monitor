<?php
class Session
{
    private $config;

    public function __construct()
    {
        $this->config       = PropFull::singleton();
        $this->Check();                                 //check de la variable de secion por defecto
    }

    public function Check()
    {
        @session_start();

        if(!isset($_SESSION['USER_DATA']))
        {
            header("Location: http://localhost/monitor/login");
        }
    }

    public function Create($dataSession)
    {
        @session_start();
        $_SESSION['USER_DATA'] = $dataSession;
    }

    public function Destroy()
    {
        @session_start();
        @session_destroy();
    }
}
?>
