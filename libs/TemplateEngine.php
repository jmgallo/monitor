<?php
class TemplateEngine
{
    private $config;
    private $ViewOff;

    public function __construct($activated)
    {
        $this->config       = PropFull::singleton();    

        $this->ViewOff     = $activated; 
    }

    public function getTemplate($template,$data)
    {
        if(!$this->ViewOff)
        {
            $index = file_get_contents($this->config->get('viewsFolder').$template.'.html');
        }
        else
        {
            $index = file_get_contents($this->config->get('viewsFolder').'Index.html'); // INDEX EN COMUN

            $file           = $this->config->get('viewsFolder').$template.'.html';

            $fileForError   = $this->config->get('viewsFolder').'ErrorPage.html';

            if(file_exists($file))
            {
                $body = file_get_contents($file);            
            }
            else
            {
                $body = file_get_contents($fileForError);
            }   
            $index = str_replace('{BODY}', $body, $index);
        }
        if(count($data) != 0)
        {
            foreach ($data as $key => $value)
            {
                $index = str_replace($key, $value, $index); 
            }
        }  
        echo $index;
    }

    /***PARA TRABAJAR CON ELEMENTO DEL TIPO SELECT */
    public function setSelect($content)
    {
        $select = "";

        foreach ($content as $key => $value)
        {
            $select .= "<option value='".$key."'>".$value."</option>";
        }

        return $select;
    }
}
?>
